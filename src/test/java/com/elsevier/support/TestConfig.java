package com.elsevier.support;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

import static org.apache.logging.log4j.LogManager.getLogger;

@Configuration
public class TestConfig {

    private static final Logger LOGGER = getLogger(TestConfig.class);

    @Bean
    WebDriver setupDriver() {
        String chromeExecutable = System.getProperty("os.name").toLowerCase().contains("windows") ? "chromedriver.exe" : "chromedriver";
        System.setProperty("webdriver.chrome.driver", this.getClass().getClassLoader().getResource("driver/" + chromeExecutable).getPath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        LOGGER.info("Initialising ChromeDriver...");
        final ChromeDriver chromeDriver = new ChromeDriver(options);
        chromeDriver.manage().window().maximize();
        chromeDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        chromeDriver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        chromeDriver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
        LOGGER.info("Initialised ChromeDriver successfully!!");
        return chromeDriver;
    }

    @Bean
    @Qualifier("baseUrl")
    String baseUrl() {
        return "http://automationpractice.com/index.php";
    }

    @Bean
    PageNavigator pageNavigator(final WebDriver driver, final @Qualifier("baseUrl") String baseUrl) {
        return new PageNavigator(driver, baseUrl);
    }

}
