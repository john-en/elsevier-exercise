package com.elsevier.support;

import com.elsevier.pageobjects.HomePage;
import org.openqa.selenium.WebDriver;

public class PageNavigator {

    WebDriver driver;
    String url;

    public PageNavigator(final WebDriver driver, final String url) {
        this.driver = driver;
        this.url = url;
    }

    public HomePage navigateToHomePage() {
        this.driver.navigate().to(this.url);
        return new HomePage(this.driver);
    }

}
