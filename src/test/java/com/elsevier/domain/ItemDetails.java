package com.elsevier.domain;

public class ItemDetails {
    String itemName;
    String sku;
    String colour;
    String size;

    public String getSize() {
        return size;
    }

    public ItemDetails setSize(String size) {
        this.size = size;
        return this;
    }

    public String getItemName() {
        return itemName;
    }

    public ItemDetails setItemName(String itemName) {
        this.itemName = itemName;
        return this;
    }

    public String getSku() {
        return sku;
    }

    public ItemDetails setSku(String sku) {
        this.sku = sku;
        return this;
    }

    public String getColour() {
        return colour;
    }

    public ItemDetails setColour(String colour) {
        this.colour = colour;
        return this;
    }

    public String toString(){
        return "itemName: " + itemName + "\nsku: " + sku + "\ncolour: " + colour + "\nsize: " + size;
    }
}
