package com.elsevier;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features",
        glue = "com.elsevier.stepdefinitions",
        tags = {"not @ignore"},
        snippets = SnippetType.CAMELCASE,
        plugin = {"pretty","usage:target/usage.json", "html:target/cucumber-reports"},
        monochrome = true)
public class CucumberE2ETest {
}
