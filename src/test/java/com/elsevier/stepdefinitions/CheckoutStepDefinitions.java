package com.elsevier.stepdefinitions;

import com.elsevier.domain.ItemDetails;
import com.elsevier.pageobjects.HomePage;
import com.elsevier.pageobjects.LoginPage;
import com.elsevier.pageobjects.SearchResultsPage;
import com.elsevier.pageobjects.ShoppingCartPage;
import com.elsevier.support.TestConfig;
import com.elsevier.support.PageNavigator;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = {TestConfig.class})
public class CheckoutStepDefinitions {

    private static final Logger LOGGER = LogManager.getLogger(CheckoutStepDefinitions.class);

    @Autowired
    PageNavigator pageNavigator;

    @Autowired
    WebDriver driver;

    //Common PageObjects
    private HomePage homePage;
    private SearchResultsPage searchResultsPage;
    private ShoppingCartPage shoppingCartPage;
    private LoginPage loginPage;

    private List<ItemDetails> expectedItemList = new ArrayList<>();

    @After
    public void tearDown(Scenario scenario){
        driver.manage().deleteAllCookies();
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }
    }

    @Given("I am on the home page")
    public void iAmOnTheHomePage() {
        homePage = pageNavigator.navigateToHomePage();
    }

    @When("I access {string} under {string}")
    public void iAccessUnder(String styleType, String category) {
        searchResultsPage = homePage.openCatalogOf(category, styleType);
    }

    @And("add the following item to cart")
    public void addTheFollowingItemToCart(DataTable table) {
        List<Map<String, String>> itemDetailsList = table.asMaps(String.class, String.class);
        itemDetailsList.forEach(itemDetail -> {
            ItemDetails item = new ItemDetails().setColour(itemDetail.get("colour"))
                    .setSku(itemDetail.get("sku"))
                    .setSize(itemDetail.get("size"));
            this.expectedItemList.add(item);
            searchResultsPage = searchResultsPage.addItemToBasket(item);
        });
    }

    @And("I proceed to checkout")
    public void iProceedToCheckout() {
        shoppingCartPage = searchResultsPage.proceedToCheckout();
    }

    @Then("I can see the item added to the basket")
    public void iCanSeeTheItemAddedToTheBasket() {
        List<ItemDetails> itemsInBasket = shoppingCartPage.getItemsInBasket();
        assertThat(itemsInBasket).hasSize(1);
        assertThat(itemsInBasket).usingElementComparatorIgnoringFields("itemName").isEqualTo(expectedItemList);
    }

    @When("I Checkout")
    public void iCheckout() {
        loginPage = shoppingCartPage.checkout();
    }

    @Then("I am directed to Login page")
    public void iAmDirectedToLoginPage() {
        assertThat(driver.getTitle()).contains("Login");
    }
}
