package com.elsevier.pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class Page {

    WebDriver driver;
    JavascriptExecutor js;

    public Page(WebDriver driver){
        this.driver = driver;
        js = (JavascriptExecutor) driver;
    }

    public void scrollToElement(WebElement element) {
        js.executeScript("arguments[0].scrollIntoView();", element);
    }

}
