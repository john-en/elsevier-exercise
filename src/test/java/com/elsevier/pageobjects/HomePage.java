package com.elsevier.pageobjects;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.apache.logging.log4j.LogManager.getLogger;

public class HomePage extends Page {

    private static final Logger LOGGER = getLogger(HomePage.class);

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(name = "Continue")
    private WebElement submit;

    /**
     * @param category
     * @param styleType
     * @return
     */
    public SearchResultsPage openCatalogOf(String category, String styleType) {
        Actions actions = new Actions(driver);
        WebElement mainMenuElement = driver.findElement(By.cssSelector(".sf-menu>li>a[title='" + category + "']"));
        Action mouseOver = actions.moveToElement(mainMenuElement).build();
        mouseOver.perform();
        LOGGER.debug("Opened menu --> '{}'", category);
        driver.findElement(By.cssSelector(".sf-menu>li>a[title='" + category + "']+ul>li>a[title='" + styleType + "']")).click();
        LOGGER.debug("Clicked submenu --> '{}", styleType);
        return new SearchResultsPage(driver);
    }
}
