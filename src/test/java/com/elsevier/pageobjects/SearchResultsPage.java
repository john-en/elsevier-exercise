package com.elsevier.pageobjects;

import com.elsevier.domain.ItemDetails;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.apache.logging.log4j.LogManager.getLogger;

public class SearchResultsPage extends Page {

    private static final Logger LOGGER = getLogger(SearchResultsPage.class);

    public SearchResultsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "[title='Proceed to checkout']")
    private WebElement proceedToCheckout;

    /**
     * Adds an item to Shopping Basket
     * @param itemDetails - an object of {@link ItemDetails} which can be used to identify and add an item to Basket
     * @return - an new instance of the same page
     */
    public SearchResultsPage addItemToBasket(ItemDetails itemDetails) {
        Actions actions = new Actions(driver);
        List<WebElement> products = driver.findElements(By.cssSelector(".product-container"));
        //Find the product using the SKU number
        for (WebElement product : products) {
            if (product.findElement(By.cssSelector(".product-name")).getAttribute("href")
                    .contains("id_product=" + itemDetails.getSku().replaceAll("[^\\d]", "").trim())) {
                scrollToElement(product);
                Action mouseOver = actions.moveToElement(product).build();
                mouseOver.perform();
                LOGGER.debug("Selected item --> '{}'", itemDetails.getSku());
                product.findElement(By.cssSelector(".quick-view span")).click();
                LOGGER.debug("Opened Item Quick-view.");
                break;
            }
        }
        WebDriverWait wait = new WebDriverWait(driver, 10);
        //Selector for the Quick View popup
        By productPopupSelector = By.cssSelector("div[itemscope]>.primary_block");
        driver.switchTo().frame(driver.findElement(By.cssSelector(".fancybox-iframe")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(productPopupSelector));
        WebElement productPopup = driver.findElement(productPopupSelector);
        //Select Colour
        LOGGER.debug("Selected colour --> '{}'", itemDetails.getColour());
        productPopup.findElement(By.cssSelector("#color_to_pick_list li>a[title='" + itemDetails.getColour() + "']")).click();
        //Select size
        LOGGER.debug("Selected size --> '{}'", itemDetails.getSize());
        Select size = new Select(productPopup.findElement(By.id("group_1")));
        size.selectByVisibleText(itemDetails.getSize());
        //Add to cart
        productPopup.findElement(By.cssSelector("#add_to_cart span")).click();
        LOGGER.debug("Added item to basket.");
        driver.switchTo().defaultContent();
        return new SearchResultsPage(driver);
    }

    /**
     * Clicks on the Proceed to Checkout' page
     * @return - an instance of the Shoppig Cart page
     */
    public ShoppingCartPage proceedToCheckout() {
        proceedToCheckout.click();
        return new ShoppingCartPage(driver);
    }
}
