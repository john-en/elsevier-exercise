package com.elsevier.pageobjects;

import com.elsevier.domain.ItemDetails;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.logging.log4j.LogManager.getLogger;

public class ShoppingCartPage extends Page {

    private static final Logger LOGGER = getLogger(ShoppingCartPage.class);

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#center_column [title='Proceed to checkout']")
    private WebElement proceedToCheckout;

    public List<ItemDetails> getItemsInBasket() {
        List <ItemDetails> itemDetails = new ArrayList<>();
        driver.findElements(By.cssSelector("td.cart_description")).forEach(cartDescription->{
            itemDetails.add(new ItemDetails().setItemName(cartDescription.findElement(By.cssSelector(".product-name")).getText())
                    .setSku(cartDescription.findElement(By.cssSelector(".cart_ref")).getText().replace("SKU :", "").trim())
                    .setColour(cartDescription.findElement(By.cssSelector("small a")).getText()
                            .split(",")[0].replace("Color :", "").trim())
                    .setSize(cartDescription.findElement(By.cssSelector("small a")).getText()
                            .split(",")[1].replace("Size :", "").trim()));

        });
        LOGGER.debug("Item in Basket -->\n {}", itemDetails.stream().map(ItemDetails::toString).collect(Collectors.toList()));
        return itemDetails;
    }

    public LoginPage checkout() {
        proceedToCheckout.click();
        return new LoginPage(driver);
    }

}
