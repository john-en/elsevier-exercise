Feature: Checkout works

  Scenario Outline: Checkout when not logged in
    Given I am on the home page
    When I access "<StyleType>" under "<Category>"
    And add the following item to cart
      | sku   | colour   | size   |
      | <sku> | <colour> | <size> |
    And I proceed to checkout
    Then I can see the item added to the basket
    When I Checkout
    Then I am directed to Login page

    Examples:
      | StyleType       | Category | sku    | colour | size |
      | Summer Dresses  | Dresses  | demo_5 | Yellow | S    |
      | Evening Dresses | Dresses  | demo_4 | Pink   | M    |
